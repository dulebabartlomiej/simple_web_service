create table profile
(
    id                 varchar(255) not null
        constraint profile_pkey
            primary key,
    created_by         varchar(255),
    created_date       timestamp,
    delete             boolean,
    last_modified_by   varchar(255),
    last_modified_date timestamp,
    version            integer,
    fb_id              varchar(255),
    linkedin_id        varchar(255)
);

create table role_type
(
    id                 varchar(255) not null
        constraint role_type_pkey
            primary key,
    created_by         varchar(255),
    created_date       timestamp,
    delete             boolean,
    last_modified_by   varchar(255),
    last_modified_date timestamp,
    version            integer,
    name               varchar(255)
        constraint uk_ouutblmtrjnx7hn1ahri6swhj
            unique
);

create table users
(
    id                 varchar(255) not null
        constraint users_pkey
            primary key,
    created_by         varchar(255),
    created_date       timestamp,
    delete             boolean,
    last_modified_by   varchar(255),
    last_modified_date timestamp,
    version            integer,
    email              varchar(255) not null
        constraint uk_6dotkott2kjsp8vw4d0m25fb7
            unique,
    name               varchar(255),
    password           varchar(255) not null,
    surname            varchar(255),
    profile_id         varchar(255) not null
        constraint fk5q3e9303ap1wvtia6sft7ht1s
            references profile
);

create table role
(
    id                 varchar(255) not null
        constraint role_pkey
            primary key,
    created_by         varchar(255),
    created_date       timestamp,
    delete             boolean,
    last_modified_by   varchar(255),
    last_modified_date timestamp,
    version            integer,
    role_type_id       varchar(255) not null
        constraint fkawrres8h1p9kky45mmwqhj86o
            references role_type,
    user_id            varchar(255)
        constraint fkgg3583634e0ydkacyk8wbbm19
            references users
);

create table oauth_access_token
(
    token_id          varchar(256),
    token             bytea,
    authentication_id varchar(256),
    user_name         varchar(256),
    client_id         varchar(256),
    authentication    bytea,
    refresh_token     varchar(256)
);

create table oauth_refresh_token
(
    token_id       varchar(256),
    token          bytea,
    authentication bytea
);

create table logs
(
    user_id varchar(20)   not null,
    dated   timestamp     not null,
    logger  varchar(50)   not null,
    level   varchar(10)   not null,
    message varchar(1000) not null
);
