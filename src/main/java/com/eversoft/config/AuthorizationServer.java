package com.eversoft.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import javax.sql.DataSource;

@Configuration
@EnableAuthorizationServer
@RequiredArgsConstructor
public class AuthorizationServer extends AuthorizationServerConfigurerAdapter {

    private static final String SECRET = "$2y$10$nkP8ZXxOAzUGqFXPlRFENuBpTeciVX4ajNeuqS0ka99oJxv.FkQsi";
    private static final String CLIENT = "client";
    private static final String SCOPE = "trust";
    private static final String GRAND_TYPE_PASSWORD = "password";
    private static final String GRAND_TYPE_REFRESH_TOKEN = "refresh_token";
    private static final String SIGNING_KEY = "KNWAdMoi3nidc1bOgcxKxrfAdabxI1cRN1wQ61ZCAc6P9lZpneI34E4DwFUXjTP/3X/YVbrgY5DKSbU1RpSQaLX3tzl8AFVg2ZweWpzUblIu8pKqLTzavnAm1JW7vvZx+0hbrpYwZqlYA51BKRWAG7HsbRqe/WSCNDaXJMJUNck=";

    private final CustomUserDetailsService customUserDetailsService;
    private final AuthenticationManager authenticationManager;
    private final DataSource dataSource;

    @Bean
    public TokenStore tokenStore() {
        return new JdbcTokenStore(dataSource);
    }

    @Bean
    public JwtAccessTokenConverter tokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setSigningKey(SIGNING_KEY);
        return converter;
    }

    @Bean
    @Primary
    public DefaultTokenServices tokenServices() {
        DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setTokenStore(tokenStore());
        defaultTokenServices.setSupportRefreshToken(true);
        return defaultTokenServices;
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) {
        security.allowFormAuthenticationForClients();
        security.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()");
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory()
                .withClient(CLIENT)
                .secret(SECRET)
                .scopes(SCOPE)
                .authorizedGrantTypes(GRAND_TYPE_PASSWORD, GRAND_TYPE_REFRESH_TOKEN)
                .refreshTokenValiditySeconds(3600)
                .accessTokenValiditySeconds(60);
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        endpoints
                .accessTokenConverter(tokenConverter())
                .reuseRefreshTokens(false)
                .userDetailsService(customUserDetailsService)
                .authenticationManager(authenticationManager)
                .tokenStore(tokenStore());
    }
}
