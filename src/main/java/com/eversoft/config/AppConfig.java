package com.eversoft.config;

import com.eversoft.dto.UserDto;
import com.eversoft.exception.NotFoundException;
import com.eversoft.model.Role;
import com.eversoft.model.RoleName;
import com.eversoft.model.User;
import com.eversoft.repository.RoleTypeRepository;
import org.modelmapper.Conditions;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Set;
import java.util.stream.Collectors;

@Configuration
public class AppConfig {
    @Autowired
    private RoleTypeRepository roleTypeRepository;
    @Bean
    public ModelMapper modelMapper() {
        ModelMapper modelMapper = new ModelMapper();

        Converter<Set<Role>, Set<Integer>> roleToEnum = ctx -> ctx.getSource() == null ? null :
                ctx.getSource().stream().map(x -> x.getRoleType().getName().ordinal()).collect(Collectors.toSet());

        Converter<Set<Integer>, Set<Role>> enumToRole = ctx -> ctx.getSource() == null ? null :
                ctx.getSource().stream().map(x -> {
                    Role role = new Role();
                    role.setRoleType(roleTypeRepository.findByName(RoleName.values()[x]).orElseThrow(NotFoundException::new));
                    return role;
                }).collect(Collectors.toSet());

        modelMapper.typeMap(User.class, UserDto.class).addMappings(mapper -> mapper.using(roleToEnum).map(User::getRoles, UserDto::setRoles));
        modelMapper.typeMap(UserDto.class, User.class).addMappings(mapper -> mapper.using(enumToRole).map(UserDto::getRoles, User::setRoles));
        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
        return modelMapper;
    }
}
