package com.eversoft.repository;

import com.eversoft.model.RoleName;
import com.eversoft.model.RoleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleTypeRepository extends JpaRepository<RoleType, String> {

    Optional<RoleType> findByName(RoleName name);
}
