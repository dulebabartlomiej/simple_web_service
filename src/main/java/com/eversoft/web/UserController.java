package com.eversoft.web;

import com.eversoft.dto.UserDto;
import com.eversoft.dto.UserFilterDto;
import com.eversoft.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Log4j
@RestController
@RequestMapping(value = "/api/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @PreAuthorize("hasAuthority('USER')")
    @GetMapping("/email/{email}")
    public ResponseEntity<UserDto> findUser(@PathVariable String email) {
        return ResponseEntity.ok(userService.getUserByEmail(email));
    }

    @PostMapping("/register")
    public ResponseEntity<UserDto> saveUser(@RequestBody @Valid UserDto userDto){
        return ResponseEntity.ok(userService.saveUser(userDto));
    }

    @PreAuthorize("hasAuthority('EDIT_PROFILE_ADMIN') or hasAuthority('EDIT_USER_ADMIN')")
    @GetMapping
    public ResponseEntity<List<UserDto>> listAllUsers(@RequestBody  UserFilterDto userFilterDto, Pageable pageable) {
        return ResponseEntity.ok(userService.getAllUsers(userFilterDto, pageable));
    }

    @PreAuthorize("hasAuthority('EDIT_PROFILE_ADMIN') or hasAuthority('EDIT_USER_ADMIN')")
    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getUserById(@PathVariable String id) {
        return ResponseEntity.ok(userService.getUserById(id));
    }

    @PreAuthorize("hasAuthority('EDIT_USER_ADMIN')")
    @PutMapping("/edit/{id}")
    public ResponseEntity<UserDto> editUserById(@PathVariable String id, @RequestBody @Valid UserDto userDto) {
        return ResponseEntity.ok(userService.editUserById(id, userDto));
    }

    @PreAuthorize("hasAuthority('EDIT_USER_ADMIN')")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<UserDto> deleteUserById(@PathVariable String id) {
        UserDto userDto = userService.deleteUserById(id);
        return ResponseEntity.ok(userDto);
    }
}
