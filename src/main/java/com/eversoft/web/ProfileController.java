package com.eversoft.web;

import com.eversoft.dto.ProfileDto;
import com.eversoft.service.ProfileService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/users/profile")
@RequiredArgsConstructor
public class ProfileController {

    private final ProfileService profileService;

    @PreAuthorize("hasAuthority('USER')")
    @PutMapping("/edit")
    public ResponseEntity<ProfileDto> editProfile(@RequestBody @Valid ProfileDto profileDto) {
        return ResponseEntity.ok(profileService.editProfile(profileDto));
    }

    @PreAuthorize("hasAuthority('EDIT_PROFILE_ADMIN')")
    @PutMapping("/edit/{username}")
    public ResponseEntity<ProfileDto> editUsersProfile(@RequestBody @Valid ProfileDto profileDto, @PathVariable String username) {
        return ResponseEntity.ok(profileService.editUsersProfile(profileDto, username));
    }

    @PreAuthorize("hasAuthority('EDIT_PROFILE_ADMIN') or hasAuthority('EDIT_USER_ADMIN')")
    @GetMapping("/{id}")
    public ResponseEntity<ProfileDto> getProfileByUserId(@PathVariable String id) {
        return ResponseEntity.ok(profileService.getProfileByUserId(id));
    }

    @PreAuthorize("hasAuthority('EDIT_PROFILE_ADMIN') or hasAuthority('EDIT_USER_ADMIN')")
    @PutMapping("/edit/id/{userId}")
    public ResponseEntity<ProfileDto> editProfileByUserId(@PathVariable String id, @RequestBody @Valid ProfileDto profileDto) {
        return ResponseEntity.ok(profileService.editProfileByUserId(id, profileDto));
    }

    @PreAuthorize("hasAuthority('EDIT_PROFILE_ADMIN') or hasAuthority('EDIT_USER_ADMIN')")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ProfileDto> deleteProfileByUserId(@PathVariable String id) {
        return ResponseEntity.ok(profileService.deleteProfileByUserId(id));
    }
}
