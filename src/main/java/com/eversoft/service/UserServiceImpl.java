package com.eversoft.service;

import com.eversoft.dto.UserDto;
import com.eversoft.dto.UserFilterDto;
import com.eversoft.exception.NotFoundException;
import com.eversoft.model.*;
import com.eversoft.repository.RoleRepository;
import com.eversoft.repository.RoleTypeRepository;
import com.eversoft.repository.UserRepository;
import com.eversoft.repository.querydsl.SearchCriteria;
import com.eversoft.repository.querydsl.UserPredicatesBuilder;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final RoleTypeRepository roleTypeRepository;
    private final ModelMapper modelMapper;
    private final TokenStore tokenStore;

    @Override
    public UserDto getUserByEmail(String email) {
        return modelMapper.map(
                userRepository.findByEmailAndDeleteIsFalse(email).orElseThrow(NotFoundException::new), UserDto.class
        );
    }

    @Override
    public UserDto saveUser(UserDto userDto) {
        User user = modelMapper.map(userDto, User.class);
        Role role = new Role();
        role.setRoleType(roleTypeRepository.findByName(RoleName.USER).orElseThrow(NotFoundException::new));
        role = roleRepository.save(role);

        Set<Role> roleSet = new HashSet<>();
        roleSet.add(role);

        role.setUser(user);
        user.setRoles(roleSet);
        user.setProfile(new Profile());
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        user.setPassword(encoder.encode(userDto.getPassword()));
        return modelMapper.map(userRepository.save(user), UserDto.class);
    }

    @Override
    public List<UserDto> getAllUsers(UserFilterDto userFilterDto, Pageable pageable) {
        UserPredicatesBuilder builder = new UserPredicatesBuilder();
        for(SearchCriteria s : userFilterDto.getSearchCriteriaList()) {
            builder = builder.with(s.getKey(), s.getOperation(), s.getValue());
        }
        return userRepository.findAll(builder.build(), pageable).stream().map(x -> modelMapper.map(x, UserDto.class)).collect(Collectors.toList());
    }

    @Override
    public UserDto getUserById(String id) {
        return modelMapper.map(userRepository.findByIdAndDeleteIsFalse(id).orElseThrow(NotFoundException::new), UserDto.class);
    }

    @Override
    public UserDto editUserById(String id, UserDto userDto) {
        User user = userRepository.findByIdAndDeleteIsFalse(id).orElseThrow(NotFoundException::new);
        UserDto userDto1 = modelMapper.map(user, UserDto.class);
        if(userDto.getRoles().containsAll(userDto1.getRoles())) {
            Collection<OAuth2AccessToken> token = tokenStore.findTokensByClientIdAndUserName("client", user.getEmail());
            token.forEach(tokenStore::removeAccessToken);
        }
        modelMapper.map(userDto, user);
        return modelMapper.map(userRepository.save(user), UserDto.class);
    }

    @Override
    public UserDto deleteUserById(String id) {
        User user = userRepository.findByIdAndDeleteIsFalse(id).orElseThrow(NotFoundException::new);
        user.setDelete(true);
        user.getProfile().setDelete(true);
        user.getRoles().forEach(role -> role.setDelete(true));
        Collection<OAuth2AccessToken> token = tokenStore.findTokensByClientIdAndUserName("client", user.getEmail());
        token.forEach(tokenStore::removeAccessToken);
        return modelMapper.map(userRepository.save(user), UserDto.class);
    }
}
