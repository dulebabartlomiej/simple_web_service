package com.eversoft.service;

import com.eversoft.dto.ProfileDto;
import com.eversoft.exception.NotFoundException;
import com.eversoft.model.Profile;
import com.eversoft.model.User;
import com.eversoft.repository.ProfileRepository;
import com.eversoft.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class ProfileServiceImpl implements ProfileService {

    private final UserRepository userRepository;
    private final ProfileRepository profileRepository;
    private final ModelMapper modelMapper;

    @Override
    public ProfileDto editProfile(ProfileDto profileDto) {
        User user = userRepository.findByEmailAndDeleteIsFalse(SecurityContextHolder.getContext().getAuthentication().getName()).orElseThrow(NotFoundException::new);
        Profile profile = user.getProfile();
        modelMapper.map(profileDto, profile);
        return modelMapper.map(profileRepository.save(profile), ProfileDto.class);
    }

    @Override
    public ProfileDto editUsersProfile(ProfileDto profileDto, String username) {
        User user = userRepository.findByEmailAndDeleteIsFalse(username).orElseThrow(NotFoundException::new);
        Profile profile = user.getProfile();
        modelMapper.map(profileDto, profile);
        return modelMapper.map(profileRepository.save(profile), ProfileDto.class);
    }

    @Override
    public ProfileDto getProfileByUserId(String id) {
        User user = userRepository.findByIdAndDeleteIsFalse(id).orElseThrow(NotFoundException::new);
        return modelMapper.map(user.getProfile(), ProfileDto.class);
    }

    @Override
    public ProfileDto editProfileByUserId(String id, ProfileDto profileDto) {
        User user = userRepository.findByIdAndDeleteIsFalse(id).orElseThrow(NotFoundException::new);
        Profile profile = user.getProfile();
        modelMapper.map(profileDto, profile);
        return modelMapper.map(profileRepository.save(profile), ProfileDto.class);
    }

    @Override
    public ProfileDto deleteProfileByUserId(String id) {
        User user = userRepository.findByIdAndDeleteIsFalse(id).orElseThrow(NotFoundException::new);
        Profile profile = user.getProfile();
        profile.setDelete(true);
        return modelMapper.map(profileRepository.save(profile), ProfileDto.class);
    }
}
