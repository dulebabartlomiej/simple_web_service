package com.eversoft.service;

import com.eversoft.dto.ProfileDto;

public interface ProfileService {

    ProfileDto editProfile(ProfileDto userDto);

    ProfileDto editUsersProfile(ProfileDto userDtoc, String userId);

    ProfileDto getProfileByUserId(String id);

    ProfileDto editProfileByUserId(String id, ProfileDto profileDto);

    ProfileDto deleteProfileByUserId(String id);

}
