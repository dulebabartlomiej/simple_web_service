package com.eversoft.service;

import com.eversoft.dto.UserDto;
import com.eversoft.dto.UserFilterDto;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserService {
    UserDto getUserByEmail(String email);

    UserDto saveUser(UserDto userDto);

    List<UserDto> getAllUsers(UserFilterDto userFilterDto, Pageable pageable);

    UserDto getUserById(String id);

    UserDto editUserById(String id, UserDto userDto);

    UserDto deleteUserById(String id);
}
