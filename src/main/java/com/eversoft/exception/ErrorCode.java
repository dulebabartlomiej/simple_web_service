package com.eversoft.exception;

public enum ErrorCode {
    NOT_FOUND(404),
    FORBIDDEN(403);

    private final int code;

    ErrorCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
