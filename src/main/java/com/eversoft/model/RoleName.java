package com.eversoft.model;

public enum RoleName {
    USER,
    EDIT_PROFILE_ADMIN,
    EDIT_USER_ADMIN
}
