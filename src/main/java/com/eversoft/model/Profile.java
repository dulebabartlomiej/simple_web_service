package com.eversoft.model;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@SuperBuilder
public class Profile extends BaseEntity {

    @Column(name = "fb_id")
    private String fbId;
    @Column(name = "linkedin_id")
    private String linkedinId;
    @OneToOne
    private User user;
}
