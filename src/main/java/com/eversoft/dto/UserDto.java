package com.eversoft.dto;

import com.eversoft.annotations.PasswordValidation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDto {
    private String id;
    @Email
    @NotBlank
    private String email;
    @PasswordValidation
    private String password;
    private String name;
    private String surname;
    private String profileId;
    private Set<Integer> roles;
}
