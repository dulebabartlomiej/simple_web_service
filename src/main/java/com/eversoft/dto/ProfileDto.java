package com.eversoft.dto;

import lombok.Data;

@Data
public class ProfileDto {
    private String id;
    private Long fbId;
    private Long linkedinId;
    private String userId;
}
