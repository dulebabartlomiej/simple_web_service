package com.eversoft.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class RoleDto {
    private String id;
    @NotBlank
    private String userId;
    @NotNull
    private Integer roleType;
}
