package com.eversoft.dto;

import com.eversoft.repository.querydsl.SearchCriteria;
import lombok.Data;

import java.util.List;

@Data
public class UserFilterDto {
    private List<SearchCriteria> searchCriteriaList;
}
