
drop table if exists profile CASCADE;
drop table if exists role CASCADE;
drop table if exists role_type CASCADE;
drop table if exists users CASCADE;
drop table if exists oauth_access_token CASCADE;
drop table if exists oauth_refresh_token CASCADE;

create table profile
(
    id                 varchar(255) not null
        constraint profile_pkey
            primary key,
    created_by         varchar(255),
    created_date       timestamp,
    delete             boolean,
    last_modified_by   varchar(255),
    last_modified_date timestamp,
    version            integer,
    fb_id              varchar(255),
    linkedin_id        varchar(255),
    user_id            varchar(255)
);

create table role_type
(
    id                 varchar(255) not null
        constraint role_type_pkey
            primary key,
    created_by         varchar(255),
    created_date       timestamp,
    delete             boolean,
    last_modified_by   varchar(255),
    last_modified_date timestamp,
    version            integer,
    name               varchar(255)
        constraint uk_ouutblmtrjnx7hn1ahri6swhj
            unique
);

create table users
(
    id                 varchar(255) not null
        constraint users_pkey
            primary key,
    created_by         varchar(255),
    created_date       timestamp,
    delete             boolean,
    last_modified_by   varchar(255),
    last_modified_date timestamp,
    version            integer,
    email              varchar(255) not null
        constraint uk_6dotkott2kjsp8vw4d0m25fb7
            unique,
    name               varchar(255),
    password           varchar(255) not null,
    surname            varchar(255),
    profile_id         varchar(255) not null
        constraint fk5q3e9303ap1wvtia6sft7ht1s
            references profile
);

create table role
(
    id                 varchar(255) not null
        constraint role_pkey
            primary key,
    created_by         varchar(255),
    created_date       timestamp,
    delete             boolean,
    last_modified_by   varchar(255),
    last_modified_date timestamp,
    version            integer,
    role_type_id       varchar(255) not null
        constraint fkawrres8h1p9kky45mmwqhj86o
            references role_type,
    user_id            varchar(255)
        constraint fkgg3583634e0ydkacyk8wbbm19
            references users
);


create table oauth_access_token
(
    token_id          varchar(256),
    token             bytea,
    authentication_id varchar(256),
    user_name         varchar(256),
    client_id         varchar(256),
    authentication    bytea,
    refresh_token     varchar(256)
);

create table oauth_refresh_token
(
    token_id       varchar(256),
    token          bytea,
    authentication bytea
);

INSERT INTO public.profile (id, created_by, created_date, delete, last_modified_by, last_modified_date, version, fb_id, linkedin_id) VALUES ('791f2bc5-5a37-4f1e-bce5-27b8374555cf', null, null, false, null, null, 0, null, null);
INSERT INTO public.profile (id, created_by, created_date, delete, last_modified_by, last_modified_date, version, fb_id, linkedin_id) VALUES ('28dd6160-94ad-401b-836f-97c76f43361b', null, null, false, null, null, 0, null, null);

INSERT INTO public.users (id, created_by, created_date, delete, last_modified_by, last_modified_date, version, email, name, password, surname, profile_id)
VALUES ('ee84d7e6-dbab-4a88-a52e-b5ecc6507b9e', null, null, false, null, null, 0, 'user@user.pl', 'name', '$2a$10$SvQE3GofZeUth2VU04jlWO.ax9e1l8CxqXRdkd3A3cH/3KJPM.eIm', 'surnamse', '791f2bc5-5a37-4f1e-bce5-27b8374555cf');
INSERT INTO public.users (id, created_by, created_date, delete, last_modified_by, last_modified_date, version, email, name, password, surname, profile_id)
VALUES ('3e44e874-aa30-427a-9945-09ad8a1bd148', null, null, false, null, null, 0, 'admin@admin.pl', 'name', '$2a$10$Qy4Qle3BOuq6/GhXBA7pR.5g1x6t13bFzK2X8EWoYldxxRDgrXcau', 'surnamse', '28dd6160-94ad-401b-836f-97c76f43361b');


INSERT INTO public.role_type (id, created_by, created_date, delete, last_modified_by, last_modified_date, version, name) VALUES ('1', null, null, null, null, null, 0, 'USER');
INSERT INTO public.role_type (id, created_by, created_date, delete, last_modified_by, last_modified_date, version, name) VALUES ('2', null, null, null, null, null, 0, 'EDIT_PROFILE_ADMIN');
INSERT INTO public.role_type (id, created_by, created_date, delete, last_modified_by, last_modified_date, version, name) VALUES ('3', null, null, null, null, null, 0, 'EDIT_USER_ADMIN');


INSERT INTO public.role (id, created_by, created_date, delete, last_modified_by, last_modified_date, version, role_type_id, user_id)
VALUES ('bcda61a4-c049-44f0-8c7a-adbcdc082ed2', null, null, false, null, null, 1, '1', 'ee84d7e6-dbab-4a88-a52e-b5ecc6507b9e');
INSERT INTO public.role (id, created_by, created_date, delete, last_modified_by, last_modified_date, version, role_type_id, user_id)
VALUES ('5e911968-dd47-4539-8ec7-c538a891774a', null, null, false, null, null, 1,'2', '3e44e874-aa30-427a-9945-09ad8a1bd148');
INSERT INTO public.role (id, created_by, created_date, delete, last_modified_by, last_modified_date, version, role_type_id, user_id)
VALUES ('bbc05604-c876-4360-9bc4-bb95b8f74f97', null, null, false, null, null, 1, '3', '3e44e874-aa30-427a-9945-09ad8a1bd148');
