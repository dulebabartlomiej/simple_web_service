package com.eversoft.web;

import com.eversoft.SimpleApplication;
import com.eversoft.SimpleApplicationTest;
import com.eversoft.dto.UserDto;
import com.eversoft.dto.UserFilterDto;
import com.eversoft.model.*;
import com.eversoft.repository.querydsl.SearchCriteria;
import com.google.gson.Gson;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletContext;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;

import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SimpleApplicationTest.class)
@ContextConfiguration(classes = {
        SimpleApplication.class,
})
@TestPropertySource(locations = "classpath:application.yml")
@ActiveProfiles("test")
@Sql(executionPhase= Sql.ExecutionPhase.BEFORE_TEST_METHOD,scripts="classpath:db/migration/init_db.sql")
public class UserControllerTest {
    @Autowired
    private WebApplicationContext context;

    protected MockMvc mockMvc;
    private RoleType roleType;
    private Role role;
    private User user;
    private Profile profile;
    private Gson gson = new Gson();

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();
        roleType = RoleType.builder()
                .name(RoleName.USER)
                .id("1")
                .delete(false)
                .build();
        role = Role.builder()
                .roleType(roleType)
                .delete(false)
                .id("1")
                .build();
        profile = Profile.builder()
                .fbId("1")
                .id("1")
                .build();
        user = User.builder()
                .email("user@user.pl")
                .delete(false)
                .name("name")
                .password("password")
                .surname("surname")
                .id("1")
                .roles(Collections.singleton(role))
                .profile(profile)
                .build();
        profile.setUser(user);
        role.setUser(user);
    }

    @Test
    public void givenContext_whenServletContext_thenItProvidesGreetController() {
        ServletContext servletContext = context.getServletContext();

        Assert.assertNotNull(servletContext);
        Assert.assertTrue(servletContext instanceof MockServletContext);
        Assert.assertNotNull(context.getBean("userController"));
    }

    @Test
    @WithMockUser(authorities = "USER")
    public void givenIncorrectEmail_whenFindUserByEmail_expectNotFoundException() throws Exception {
        ResultActions resultActions = mockMvc.perform(
                get("/api/users/email/user@udser.pl")
        ).andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(authorities = "EDIT_USER_ADMIN")
    public void givenIncorrectIdAndValidDto_whenEditUserById_expectNotFoundEXception() throws Exception {
        UserDto userDto = UserDto.builder()
                .email("user@user.pl")
                .id("ee84d7e6-dbab-4a88-a52e-b5ecc6507b9e")
                .password("Password!2")
                .surname("surname2")
                .roles(Collections.singleton(1))
                .name("name")
                .build();
        ResultActions resultActions = mockMvc.perform(
                put("/api/users/edit/incorrect")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .characterEncoding("UTF-8")
                        .content(gson.toJson(userDto, UserDto.class))
        ).andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(authorities = "EDIT_USER_ADMIN")
    public void givenCorrectIdAndValidDto_whenEditUserById_expectStatusOk() throws Exception {
        UserDto userDto = UserDto.builder()
                .email("user@user.pl")
                .id("ee84d7e6-dbab-4a88-a52e-b5ecc6507b9e")
                .password("Password!2")
                .surname("surname2")
                .roles(Collections.singleton(1))
                .name("name")
                .build();
        ResultActions resultActions = mockMvc.perform(
                put("/api/users/edit/ee84d7e6-dbab-4a88-a52e-b5ecc6507b9e")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .characterEncoding("UTF-8")
                        .content(gson.toJson(userDto, UserDto.class))
        ).andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = "EDIT_USER_ADMIN")
    public void givenCorrectDto_whenListAllUsers_expectStatusOkAndValidResult() throws Exception {
        UserFilterDto userFilterDto = new UserFilterDto();
        userFilterDto.setSearchCriteriaList(Collections.singletonList(new SearchCriteria("name", ":", "name")));
        UserDto userDto = UserDto.builder()
                .email("admin@admin.pl")
                .id("3e44e874-aa30-427a-9945-09ad8a1bd148")
                .password("$2a$10$Qy4Qle3BOuq6/GhXBA7pR.5g1x6t13bFzK2X8EWoYldxxRDgrXcau")
                .surname("surnamse")
                .roles(Collections.singleton(1))
                .profileId("28dd6160-94ad-401b-836f-97c76f43361b")
                .roles(Stream.of(1,2).collect(Collectors.toSet()))
                .name("name")
                .build();
        ResultActions resultActions = mockMvc.perform(
                get("/api/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .characterEncoding("UTF-8")
                        .content(gson.toJson(userFilterDto, UserFilterDto.class))
                        .param("page", "1")
                        .param("size", "1")
        ).andExpect(status().isOk());
        MockHttpServletResponse response = resultActions.andReturn().getResponse();
        String content = response.getContentAsString();
        Assert.assertEquals("[" + gson.toJson(userDto, UserDto.class) + "]", content);
    }

    @Test
    @WithMockUser(authorities = "EDIT_USER_ADMIN")
    public void givenCorrectId_whenDeleteUserBuId_expectStatusOk() throws Exception {
        String id = "ee84d7e6-dbab-4a88-a52e-b5ecc6507b9e";
        ResultActions resultActions = mockMvc.perform(
                delete("/api/users/delete/" + id)
        ).andExpect(status().isOk());
    }
}
