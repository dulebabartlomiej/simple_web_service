package com.eversoft.service;

import com.eversoft.dto.UserDto;
import com.eversoft.dto.UserFilterDto;
import com.eversoft.exception.NotFoundException;
import com.eversoft.model.*;
import com.eversoft.repository.RoleRepository;
import com.eversoft.repository.RoleTypeRepository;
import com.eversoft.repository.UserRepository;
import com.eversoft.repository.querydsl.SearchCriteria;
import com.querydsl.core.types.Predicate;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class UserServiceTest {
    @Mock
    private UserRepository userRepository;
    @Mock
    private RoleRepository roleRepository;
    @Mock
    private RoleTypeRepository roleTypeRepository;
    @Mock
    private ModelMapper modelMapper;
    @Mock
    private TokenStore tokenStore;
    private UserService userService;
    private RoleType roleType;
    private Role role;
    private User user;
    private Profile profile;
    private UserDto userDto;

    @Before
    public void setUp() {
        userService = new UserServiceImpl(userRepository, roleRepository, roleTypeRepository, modelMapper, tokenStore);
        roleType = RoleType.builder()
                .name(RoleName.USER)
                .id("1")
                .delete(false)
                .build();
        role = Role.builder()
                .roleType(roleType)
                .delete(false)
                .id("1")
                .build();
        profile = Profile.builder()
                .fbId("1")
                .id("1")
                .build();
        user = User.builder()
                .email("user@user.pl")
                .delete(false)
                .name("name")
                .password("password")
                .surname("surname")
                .id("1")
                .roles(Collections.singleton(role))
                .profile(profile)
                .build();
        userDto = UserDto.builder()
                .email("user@user.pl")
                .name("name")
                .password("password")
                .surname("surname")
                .id("1")
                .roles(Collections.singleton(0))
                .profileId("1")
                .build();
        profile.setUser(user);
        role.setUser(user);
        when(roleTypeRepository.findByName(RoleName.USER))
                .thenReturn(Optional.of(roleType));
        when(userRepository.findByIdAndDeleteIsFalse("1"))
                .thenReturn(Optional.of(user));
        when(userRepository.save(Mockito.any(User.class)))
                .thenAnswer(i -> i.getArguments()[0]);
        when(modelMapper.map(any(UserDto.class), Mockito.eq(User.class)))
                .thenReturn(user);
        when(modelMapper.map(any(User.class), Mockito.eq(UserDto.class)))
                .thenReturn(userDto);
    }

    @Test
    public void testFindUserByEmail() {
        final String id = "1";
        final String email = "user@user.pl";

        when(userRepository.findByEmailAndDeleteIsFalse(email))
                .thenReturn(Optional.of(User.builder()
                        .id("1")
                        .email(email)
                        .build()));

        UserDto userDto = userService.getUserByEmail(email);

        Assert.assertEquals(email, userDto.getEmail());
        Assert.assertEquals(id, userDto.getId());
    }

    @Test
    public void testEditUserById() {
        String id = "1";
        UserDto userDto = UserDto.builder()
                .email("u1dser@user.pl")
                .id("1")
                .password("password2")
                .surname("surname2")
                .roles(Collections.singleton(0))
                .profileId("1")
                .name("name")
                .build();
        User user2 = User.builder()
                .email("u1dser@user.pl")
                .delete(false)
                .name("name")
                .password("password2")
                .surname("surname2")
                .id("1")
                .roles(Collections.singleton(role))
                .profile(profile)
                .build();
        when(modelMapper.map(any(UserDto.class), Mockito.eq(User.class)))
                .thenReturn(user2);
        when(modelMapper.map(Mockito.any(User.class), Mockito.eq(UserDto.class)))
                .thenReturn(userDto);

        UserDto userDto1 = userService.editUserById(id, userDto);
        Assert.assertEquals(userDto, userDto1);
    }

    @Test
    public void testDeleteUserById() {
        String id = "1";

        when(userRepository.findByIdAndDeleteIsFalse(id))
                .thenReturn(Optional.of(user));

        Assert.assertEquals(false, user.getDelete());
        UserDto userDto = userService.deleteUserById(id);
        Assert.assertEquals(true, user.getDelete());
    }

    @Test
    public void testGetAllUsers() {
        UserFilterDto userFilterDto = new UserFilterDto();
        userFilterDto.setSearchCriteriaList(Collections.singletonList(new SearchCriteria("name", ":", "name")));
        Pageable pageable = PageRequest.of(0, 5);

        when(userRepository.findAll(any(Predicate.class), any(Pageable.class))).thenReturn(new PageImpl<>(Collections.singletonList(user)));
        List<UserDto> userDtoList = userService.getAllUsers(userFilterDto, pageable);

        Assert.assertEquals(1, userDtoList.size());
        Assert.assertEquals(userDto, userDtoList.get(0));
    }
}
